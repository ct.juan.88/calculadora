package org.villablanca;

import java.util.Scanner;

public class Calculadora {

	private static Scanner teclado = new Scanner(System.in);

	public static void main(String[] args) {
		opcionMenu();
	}

	public static int menu() {
		int opcion = 0;
		do {
			System.out.println("\nSelecciona una operación: ");
			System.out.println("\t1.- Sumar");
			System.out.println("\t2.- Restar");
			System.out.println("\t3.- Multiplicar");
			System.out.println("\t4.- Dividir");
			System.out.println("\t5.- Salir del programa");
			opcion = teclado.nextInt();
		} while (opcion < 1 || opcion > 5);
		return opcion;
	}

	public static void opcionMenu() {
		int optMenu;
		do {
			optMenu = menu();
			switch (optMenu) {
			case 1:
				suma();
				break;
			case 2:
				resta();
				break;
			case 3:
				multiplicar();
				break;
			case 4:
				System.out.println("Resultado: " + dividir());
				break;
			default:
				System.out.println("Fin del programa.");
			}
		} while (optMenu != 5);
	}

	public static double dividir() {
		double resultado;
		int numDivisores;
		double num;
		System.out.println("¿Cuantos números va a querer dividir?");
		numDivisores = teclado.nextInt();
		System.out.println("Dime el primer número");
		resultado = teclado.nextDouble();
		for (int i = 0; i < numDivisores - 1; i++) {
			System.out.println("Dime el siguiente número");
			num = teclado.nextDouble();
			resultado = resultado / num;
		}

		return resultado;
	}

	public static void multiplicar() {
		System.out.println("Dame el primer numero: ");
		double a = teclado.nextDouble();
		System.out.println("Dame el segundo numero: ");
		double b = teclado.nextDouble();
		System.out.println("Resultado: " + (a*b));
	}

	public static void resta() {
		System.out.println("Dime el primer numero");
		double numero1 = teclado.nextDouble();
		System.out.println("Dame el segundo numero");
		double numero2 = teclado.nextDouble();
		System.out.println("El resultado es: " + (numero1 - numero2));
	}

	public static void suma() {
		System.out.println("Primer valor: ");
		double a = teclado.nextDouble();
		System.out.println("Segundo valor: ");
		double b = teclado.nextDouble();
		System.out.println("Resultado: " + (a+b));
	}

}
